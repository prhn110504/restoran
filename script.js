const dataMenu = [
	{
		nama: 'Kepiting Goreng',
		harga: 'Rp 45.000',
		gambar: 'foto/Kepitinggoreng.png'
	}, {
		nama: 'Nasi Goreng',
		harga: 'Rp 50.000',
		gambar: 'foto/nasigoreng.jpg'
	}, {
		nama: 'Sayur Seafood',
		harga: 'Rp 45.000',
		gambar: 'foto/sayur.jpg'
	}, {
		nama: 'Sup Gurame',
		harga: 'Rp 60.000',
		gambar: 'foto/supgurame.jpg'
	}, {
		nama: 'Udang Asam Manis',
		harga: 'Rp 45.000',
		gambar: 'foto/udang.jpg'
	}, {
		nama: 'Udang Tepung',
		harga: 'Rp 43.000',
		gambar: 'foto/udangtepung.jpg'
	}, {
		nama: 'Udang Asam Manis',
		harga: 'Rp 47.000',
		gambar: 'foto/udang.webp'
	}, {
		nama: 'Kepiting Asam Manis',
		harga: 'Rp 50.000',
		gambar: 'foto/kepiting.jpg'
	}, {
		nama: 'Cumi Asam Manis',
		harga: 'Rp 45.000',
		gambar: 'foto/cumi.png'
	}, {
		nama: 'Gurame Asam Manis',
		harga: 'Rp 52.000',
		gambar: 'foto/gurame.png'
	},
];



//map
const loopMenu = (item, index, array) =>{
	const namaItem		= item.nama.toLowerCase();
	let namaContainer;
		(namaItem.includes('menu')) 
		
		namaContainer = document.querySelector('.container-menu');
		namaContainer.innerHTML += `
	<div class="card card-promo">
             <div class="animate-img">
                 <img src="${item.gambar}" class="card-img-top img-fluid" alt="card1">
             </div>
             <div class="card-body">
                 <h5 class="card-title">${item.nama}</h5>
                 <p class="card-text">${item.harga}</p>
                <a href="#" class="btn btn-outline-dark">Pesan</a>
            </div>
        </div>
	`
}
dataMenu.map(loopMenu);


//filter

const inputKeyword = document.querySelector('.input-keyword')
.addEventListener('input', (event) => {
	const keyword = event.target.value.toLowerCase();
	const containerMilkshake = document.querySelector('.container-milkshake')
	const cariMenu = (item, index)=>{
		return item.nama.toLowerCase().includes(keyword);
	}

	const hasilMenus = dataMenu.filter(cariMenu);

	document.querySelector('.container-menu').innerHTML = '';

	hasilMenus.map(loopMenu);


});
